#include <iostream>
#include <stdlib.h>

using namespace std;

int main(){
      system("clear");
      
      int tinggi;
      
      cout << "N : ";
      cin >> tinggi;

      if (tinggi % 2 == 0) {
          
          cout << "Masukkan angka ganjil" << endl;
          
          return 1;
        }

    for (int i = 1; i <= tinggi; i += 2) {
        for (int j = 0; j < (tinggi - i) / 2; ++j) {
            cout << " ";
        }

        for (int k = 0; k < i; ++k) {
            cout << "*";
        }

        cout << endl;
    }

    for (int i = tinggi - 2; i >= 1; i -= 2) {
        for (int j = 0; j < (tinggi - i) / 2; ++j) {
            cout << " ";
        }

        for (int k = 0; k < i; ++k) {
            cout << "*";
        }

        cout << std::endl;
    }

    return 0;
}
